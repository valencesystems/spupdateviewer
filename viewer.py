from flask import Flask
from flask import render_template
app = Flask(__name__)

@app.route("/audio")
def audio():
    return render_template('video.html')

@app.route("/video")
def video():
    return render_template('video.html')

@app.route("/image")
def image():
    return render_template('video.html')

if __name__ == "__main__":
    app.run()


